export interface IChromeTab {
    active: boolean;
    id: number;
    highlighted: boolean;
    url: string;
    windowId: number;
}

interface IChromeWindow {
    id: number;
    focused: boolean;
}

interface IMessageSender {
    id?: string;
    frameId?: number;
}

interface IChromeOnmessageListener {
    (message: any, sender?: IMessageSender, sendResponse?: Function): void;
}

interface INativeChromeV2Manifest {
    tabs: {
        create(properties: Partial<IChromeTab>, resolve: Function): Promise<IChromeTab>;
        update(tabId: number, properties: Partial<IChromeTab>, callback: Function): Promise<IChromeTab>;
        remove(tabId: number, callback: Function): Promise<IChromeTab>;
        executeScript(tabId: number, options: { code: string }, callback: Function): void;
        query(properties: Partial<IChromeTab>, callback: Function): Promise<IChromeTab[]>;
    };
    windows: {
        update(windowId: number, properties: Partial<IChromeWindow>, callback: Function): Promise<IChromeTab>;
    };
    runtime: {
        getURL(url: string): string;
        onMessage: {
            addListener(listener: IChromeOnmessageListener): void;
            removeListener(listener: IChromeOnmessageListener): void;
        };
        sendMessage(extensionId: string, data: any): void;
    };
}

function smartToString(entry: Function | string | number | object) {
    switch (typeof entry) {
        case 'function':
            return entry.toString();
        case 'string':
            return `'${entry}'`;
        case 'number':
            return entry;
        case 'object'://todo: checks for different cases
            return JSON.stringify(entry);
        default:
            throw new Error('smart to string not implemented');
    }
}

const chunkRe = /\(\d+,[^.]+\.[^)]+\)/gim;
const leftRe = /\(\d+,[^.]+\./gim;
const rightRe = /\)/gim;


export function compose(fn: Function, ...provides: any[]): Function {
    return (resolve: Function, ...params: any[]) => {

        let string = `(() => {
            //Provides
            ${[...provides].map(smartToString).join(';\n\n')}

            //Invocation
            (${smartToString(fn)}).call(this, ${resolve}, ${[...params].map(smartToString).join(', ')});
        })();`;

        const parts = string.split(chunkRe);
        const matches = string.match(chunkRe) || [];

        return parts.map((item, index) => {
            const tail = matches[index] ? matches[index]
                .replace(leftRe, '')
                .replace(rightRe, '') : '';
            return item + tail;
        }).join('');
    }
}

declare const chrome: INativeChromeV2Manifest;

interface IChromeApi {
    tab: {
        open: { (url: string): Promise<IChromeTab> };
        singleton: { (url: string): Promise<IChromeTab> };
        focus: { (tab: IChromeTab): Promise<IChromeTab> };
        close: { (tab: IChromeTab): Promise<IChromeTab> };
    };
    execute: {
        asContextScript: { <T>(tabId: number, fn: Function, ...args: any[]): Promise<T> }
    };
}

export const chromeApi: IChromeApi = {
    tab: {
        open: (url: string) => new Promise<IChromeTab>(resolve => chrome.tabs.create({
            url,
            active: false
        }, resolve)),
        focus: (tab: IChromeTab) => new Promise<IChromeTab>(async resolve => {
            await chrome.windows.update(tab.windowId, {focused: true}, () => null);
            await chrome.tabs.update(tab.id, {active: true}, resolve);
        }),
        close: (tab: IChromeTab) => new Promise<IChromeTab>(resolve => chrome.tabs.remove(tab.id, resolve)),
        singleton: async (url: string) => {

            const found: IChromeTab[] = await new Promise(
                resolve => chrome.tabs.query({url: url + '/*'}, (data: IChromeTab[]) => {
                    resolve(data);
                }));

            if (found.length > 0) {
                return found[0];
            }
            return chromeApi.tab.open(url);
        }
    },
    execute: {
        asContextScript: <T>(tabId: number, fn: Function, ...args: any[]) => {
            //todo: promise rejection
            return new Promise<T>((resolve) => {
                const random = Math.random().toString();
                const extensionId = chrome.runtime.getURL('')
                    .replace('chrome-extension://', '')
                    .replace('/', '');
                const listener = (message: { random: string, payload: any }) => {
                    let {random: randomSeed, payload} = message;
                    if (randomSeed == random) {
                        chrome.runtime.onMessage.removeListener(listener);
                        resolve(payload);
                    }
                };
                chrome.runtime.onMessage.addListener(listener);

                const acknowledgeExtension = ((payload: any) => chrome.runtime.sendMessage('`${extensionId}`', {
                    random: `${random}`,
                    payload: payload
                }))
                    .toString()
                    .replace('`${random}`', random)
                    .replace('`${extensionId}`', extensionId);

                const code = fn(acknowledgeExtension, ...args);

                chrome.tabs.executeScript(tabId, {code}, () => {
                });
            })
        }
    }
};